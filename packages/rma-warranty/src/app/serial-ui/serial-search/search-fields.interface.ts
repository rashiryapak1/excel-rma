export interface SerialSearchFields {
  serial_no?: string;
  item_code?: string;
  item_name?: string;
  warehouse?: string;
  purchase_document_no?: string;
  delivery_note?: string;
  customer?: string;
  supplier?: string;
}
